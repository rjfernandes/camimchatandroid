package app.mrfsolution.camimchat.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import app.mrfsolution.camimchat.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by robsonfernandes on 13/01/17.
 */

public class DoctorAdapter extends BaseAdapter {

    @BindView(R.id.lblName)
    TextView lblName;

    private Context context;
    List<Map<String, Object>> items = new ArrayList<>();

    public DoctorAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Map<String, Object> getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View cell, ViewGroup parent) {
        cell = LayoutInflater.from(context).inflate(R.layout.item_doctor, parent, false);
        ButterKnife.bind(this, cell);

        Map<String, Object> item = getItem(position);

        lblName.setText(item.get("name").toString());

        return cell;
    }

    public void addItem(Map<String, Object> map) {
        items.add(map);
        notifyDataSetChanged();
    }
}
