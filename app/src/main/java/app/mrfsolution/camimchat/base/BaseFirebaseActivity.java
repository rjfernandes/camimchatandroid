package app.mrfsolution.camimchat.base;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by robsonfernandes on 13/01/17.
 */

public class BaseFirebaseActivity extends AppCompatActivity {
    protected DatabaseReference dbReference() {
        return FirebaseDatabase.getInstance().getReference();
    }

    protected void runActivity(Class<?> aclass) {
        Intent intent = new Intent(this, aclass);
        startActivity(intent);
    }
}
