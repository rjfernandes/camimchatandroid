package app.mrfsolution.camimchat.activities;

import android.os.Bundle;
import android.widget.EditText;

import app.mrfsolution.camimchat.R;
import app.mrfsolution.camimchat.base.BaseFirebaseActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseFirebaseActivity {

    @BindView(R.id.edtName)
    EditText edtName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btnEntrar)
    public void actEntrar() {
        String value = edtName.getText().toString().trim();
        if (value.equals("")) {
            return;
        }
        PickDoctorActivity.name = value;
        runActivity(PickDoctorActivity.class);
    }
}
