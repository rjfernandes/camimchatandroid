package app.mrfsolution.camimchat.activities;

import android.os.Bundle;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import java.util.Map;

import app.mrfsolution.camimchat.R;
import app.mrfsolution.camimchat.adapter.DoctorAdapter;
import app.mrfsolution.camimchat.base.BaseFirebaseActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PickDoctorActivity extends BaseFirebaseActivity {

    public static String name;

    DatabaseReference doctorsRef = dbReference().child("anchieta").child("physicians");
    ChildEventListener childEventListener;

    @BindView(R.id.lstDoctor)
    ListView lstDoctor;

    DoctorAdapter doctorAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_doctor);
        ButterKnife.bind(this);

        doctorAdapter = new DoctorAdapter(this);
        lstDoctor.setAdapter(doctorAdapter);

        childEventListener = doctorsRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (!(dataSnapshot.getValue() instanceof Map)) {
                    return;
                }

                final Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
                map.put("id", dataSnapshot.getKey());

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        doctorAdapter.addItem(map);
                    }
                });
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
